import 'package:flutter/material.dart';

class RssColors {
  static const Color black = Colors.black;
  static const Color white = Colors.white;
  static const Color grey = Colors.black38;
  static const Color red = Colors.redAccent;
  static const Color purple0 = Colors.deepPurple;
}
