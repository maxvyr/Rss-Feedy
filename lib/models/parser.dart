import 'dart:async';
import 'package:flutter/material.dart';
import 'package:webfeed/webfeed.dart';
import 'package:http/http.dart';

class Parser {
  //Variable
  final url = "http://www.frandroid.com/feed";

  Future chargerRSS() async {
    //recover url server with package http
    final response = await get(
      Uri.parse(url),
    );
    // if response 200 ok => enregistrer le feed (body) avec pacakge webFeed
    // else print erreur code
    if (response.statusCode == 200) {
      final feed = RssFeed.parse(response.body);
      debugPrint(feed.title);
      return feed;
    } else {
      debugPrint("erreur serveur ${response.statusCode}");
    }
  }
}
