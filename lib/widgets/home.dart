import 'package:flutter/material.dart';
import 'package:flutter_application_rss_feed/models/parser.dart';
import 'package:flutter_application_rss_feed/widgets/chargement.dart';
import 'package:flutter_application_rss_feed/widgets/grid_items.dart';
import 'package:flutter_application_rss_feed/widgets/list_items.dart';
import 'package:rss_feed_theme/rss_feed_theme.dart';
import 'package:webfeed/domain/rss_feed.dart';
import 'dart:async';

class Home extends StatefulWidget {
  const Home({
    required this.title,
    Key? key,
  }) : super(key: key);

  final String title;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  //Variable
  RssFeed? feed;

  @override
  void initState() {
    super.initState();
    parse();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.title),
        actions: [
          IconButton(
              icon: const Icon(
                Icons.refresh,
                color: RssColors.white,
              ),
              onPressed: () {
                //update list
                setState(() {
                  parse();
                });
              })
        ],
      ),
      body: choixDuBody(),
    );
  }

  Widget choixDuBody() {
    if (feed == null) {
      return const Chargement();
    } else {
      Orientation orientation = MediaQuery.of(context).orientation;
      if (orientation == Orientation.portrait) {
        //List
        return ListItems(feed: feed!);
      } else {
        //Grid
        return GridItems(feed: feed!);
      }
    }
  }

  Future<void> parse() async {
    RssFeed recoverFeed = await Parser().chargerRSS();
    if (recoverFeed != null) {
      setState(() {
        feed = recoverFeed;
        // debugPrint('${feed!.items!.length}');
        feed?.items?.forEach((element) {
          // debugPrint(element.title);
          // debugPrint(element.description);
          // debugPrint('${element.pubDate}');
        });
      });
    } else {
      debugPrint("rien reçu");
    }
  }
}
