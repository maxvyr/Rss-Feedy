import 'package:flutter/material.dart';
import 'package:flutter_application_rss_feed/widgets/my_text.dart';
import 'package:rss_feed_theme/rss_feed_theme.dart';

class Chargement extends StatelessWidget {
  const Chargement({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: MyText(
        data: "Chargement en cours....",
        fontWeight: FontWeight.w900,
        color: RssColors.black,
      ),
    );
  }
}
