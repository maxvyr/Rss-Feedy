import 'package:flutter/material.dart';
import 'package:rss_feed_theme/rss_feed_theme.dart';

class MyText extends Text {
  MyText({
    required String data,
    Color color = RssColors.purple0,
    double fontSize = 16.0,
    FontWeight fontWeight = FontWeight.normal,
  }) : super(
          //if data null replace by an empty string
          data,
          style: TextStyle(
            color: color,
            fontWeight: fontWeight,
            fontSize: fontSize,
          ),
        );
}
