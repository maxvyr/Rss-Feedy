import 'package:flutter/material.dart';
import 'package:flutter_application_rss_feed/models/url_launcher.dart';
import 'package:flutter_application_rss_feed/widgets/my_text.dart';
import 'package:rss_feed_theme/rss_feed_theme.dart';
import 'package:webfeed/webfeed.dart';

class ListItems extends StatefulWidget {
  final RssFeed feed;

  //Constructor
  const ListItems({
    required this.feed,
    Key? key,
  }) : super(key: key);

  @override
  _ListItemsState createState() => _ListItemsState();
}

class _ListItemsState extends State<ListItems> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        //recover number of item in the RSS
        itemCount: widget.feed.items!.length,
        itemBuilder: (context, position) {
          RssItem item = widget.feed.items![position];
          final date = "${item.pubDate?.year} / ${item.pubDate?.month}"
              " / "
              "${item.pubDate?.day}";
          return Container(
            padding: const EdgeInsets.only(
              left: RssConstants.marginDefault,
              right: RssConstants.marginDefault,
              bottom: RssConstants.marginDefault,
            ),
            child: Card(
              elevation: 10,
              child: InkWell(
                onTap: () {
                  launchInBrowser(item.link ?? "");
                },
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(RssConstants.marginDefault),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          // MyText(data: item.)
                          MyText(
                            data: date,
                            fontSize: 8.0,
                            color: RssColors.red,
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(RssConstants.marginLittle),
                      child: MyText(
                        data: item.title ?? "No Title",
                        color: RssColors.black,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(RssConstants.marginLittle),
                      child: MyText(
                        data: item.description ?? "No Description",
                        fontSize: 12.0,
                        color: RssColors.grey,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
